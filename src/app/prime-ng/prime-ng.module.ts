import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// CommonModule module que contiene las directivas ngIf ngFor pipes etc.

//PrimeNg
import { ButtonModule  } from 'primeng/button';
import { CardModule    } from 'primeng/card';
import { MenubarModule } from 'primeng/menubar';
import { FieldsetModule } from 'primeng/fieldset';
import { ToolbarModule } from 'primeng/toolbar';
import { TableModule   } from 'primeng/table';



@NgModule({
  declarations: [],
  // imports: [//no es necesario realizar el imports de los módulos ButtonModule y CardModule
  //ya que el módulo PrimeNgModule solo sirve para exportar los módulos que utilicemos del PrimeNg
  //   // CommonModule,
  //   ButtonModule,
  //   CardModule
  // ],
  exports: [
    ButtonModule,
    CardModule,
    MenubarModule,
    FieldsetModule,
    ToolbarModule,
    TableModule
  ]
})
export class PrimeNgModule { }
