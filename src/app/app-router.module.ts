import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BasicosComponent } from './ventas/pages/basicos/basicos.component';
import { NumerosComponent } from './ventas/pages/numeros/numeros.component';
import { NoComunesComponent } from './ventas/pages/no-comunes/no-comunes.component';
import { OrdenarComponent } from './ventas/pages/ordenar/ordenar.component';


const routes: Routes = [
  {
    path: '',
    component: BasicosComponent,
    pathMatch: 'full'//pq solo cuando esté el path vacio se va mostrar el componente BasicosComponent
  },
  {
    path: 'numeros',
    component: NumerosComponent
  },
  {
    path: 'no-comunes',
    component: NoComunesComponent
  },
  {
    path: 'ordenar',
    component: OrdenarComponent
  },
  {
    path: '**',//el comodin para asegurarnos que si no están ninguno de los path anteriores nos redirige a un tab vacio al componente basico en todo caso (declarado al inicio)
    redirectTo: ''
  },
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)//forRoot porque son las rutas principales
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }
