import { Component, OnInit } from '@angular/core';
import { from, interval, tap } from 'rxjs';

interface persona {
  nombre: string;
  genero: string;
}

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})

export class NoComunesComponent {

  //i18nSelect

  nombre: string = 'Marina';
  genero: string = 'femenino';

  personas: persona[] = [
    { nombre: 'Jaime', genero: 'masculino' },
    { nombre: 'Marina', genero: 'femenino' }
  ]

  invitacionMap = {
    'masculino': 'invitarlo',
    'femenino' : 'invitarla'
  }

  //i18nPlural
  clientes: string[] = ['Carmen','Jose','Royer','Mauricio','Raul'];

  clientMap = {
    '=0' : 'No tenemos ningún cliente esperando',
    '=1' : 'tenemos un cliente esperando',
    'other' : 'tenemos # clientes esperando',
  }

  cambiarCliente = ()=> {
    this.nombre = 'Raul';
    this.genero = 'masculino';
  }

  borrarCliente = () => {
    this.clientes.splice(0,1);
  }

  persona = {
    nombre: 'Diana Ysabel',
    edad: 31,
    direccion: 'Montreal, Canada'
  }

  numeros = [1,8,10,30,-5];
  values = '';
  
  obs01$ = from(this.numeros);
  obs02$ = interval(1000).pipe(tap(resp=> console.log('tap', resp)));

  valorPromesa = new Promise( (resolve, reject) => {
    setTimeout(() => {
      resolve('Fin de la promesa');
    }, 3500);
  }

  );
}
