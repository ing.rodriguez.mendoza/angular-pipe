import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styles: [
  ]
})
export class NumerosComponent implements OnInit {

  ventasNetas: number = 5562.55455;
  porcentaje: number = 0.53;

  constructor() { }

  ngOnInit(): void {
  }

}
