import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {

  nombreLower: string = 'diana';
  nombreUpper: string = 'DIANA';
  nombreCompleto: string = 'diaNa ySABEL';
  fecha: Date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

}
