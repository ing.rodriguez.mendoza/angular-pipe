import { Component, OnInit } from '@angular/core';
import { Color, Heroe } from '../../interfaces/ventas.interfaces';

@Component({
  selector: 'app-ordenar',
  templateUrl: './ordenar.component.html',
  styles: [
  ]
})
export class OrdenarComponent implements OnInit {

  enMayusculas : boolean = false;
  changeBy : string = '';
  data: string[]  = [];
  heroes: Heroe[] = [
    {
    nombre: 'Superman',
    vuela: true,
    color: Color.azul
    },
    {
      nombre: 'Batman',
      vuela: false,
      color: Color.negro
    },
    {
      nombre: 'Robin',
      vuela: false,
      color: Color.verde
    },
    {
      nombre: 'Daredevil',
      vuela: false,
      color: Color.rojo
    },
    {
      nombre: 'Linterna Verde',
      vuela: false,
      color: Color.verde
    },
    {
      nombre: 'Bubula',
      vuela: true,
      color: Color.verde
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  changeCase = () => 
    // this.enMayusculas? this.enMayusculas = false : this.enMayusculas = true;
    this.enMayusculas = !this.enMayusculas;
  

  changeOrder = (value: string) => {
    this.changeBy = value;
    console.log('this.changeBy',this.changeBy);}
  
}
