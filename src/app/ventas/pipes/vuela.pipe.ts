import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name:'fly'
})
export class VuelaPipe implements PipeTransform {
    transform(value: boolean) {
        return value? 'si': 'no';
    }
}