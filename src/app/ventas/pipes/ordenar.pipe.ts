import { Pipe, PipeTransform } from '@angular/core';
import { Color, Heroe } from '../interfaces/ventas.interfaces';

@Pipe({
  name: 'ordenar'
})
export class OrdenarPipe implements PipeTransform {
  
  
  compare =  (a: Heroe, b: Heroe) => {
    return (a.nombre < b.nombre)? -1 : 1;
    // a debe ser igual b
    // return 0;
  }

  transform(heroes: Heroe[], valor: string = 'sin valor'): Heroe[] {
    switch (valor) {
      case 'nombre':
        heroes = heroes.sort( (a, b) => a.nombre < b.nombre? -1 : 1 );
        break;
      case 'vuela':
        heroes = heroes.sort( (a, b) => a.vuela < b.vuela? -1 : 1 );
        break;      
      case 'color':
        heroes = heroes.sort( (a, b) => a.color < b.color? -1 : 1 );
        break;        
      default:
        break;
    }
    return heroes;
    //otther way
    // console.log('heroes', heroes.sort( this.compare ));
    // return heroes.sort( this.compare );
    //valores fijos de prueba
    // return [{
    //   nombre:'Jaime',
    //   vuela: true,
    //   color: Color.azul
    // }];
  }
}
