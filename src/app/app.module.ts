import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
// import { PrimeNgModule } from './prime-ng/prime-ng.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';//est libreria se importa cuando tenemos algun componente que contenga animación

import { AppRouterModule } from './app-router.module';
import { SharedModule } from './shared/shared.module';
import { VentasModule } from './ventas/ventas.module';


//Cambiar el locale de la app
import localeEs  from '@angular/common/locales/es-PE';
import localeFr  from '@angular/common/locales/fr';
import { registerLocaleData } from "@angular/common"; //funcion para registrar los idiomas para la app

registerLocaleData( localeEs );
registerLocaleData( localeFr );

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // PrimeNgModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRouterModule,//se importa aqui pq este módulo necesita saber las rutas emi aplicacion   
    SharedModule,
    VentasModule//se importa este módulo aqui ya que no teemos implementado el lazyload
  ],
  providers: [
    { provide: LOCALE_ID, useValue:'es-PE' },//para cambiar el idioma de forma global
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'S/.'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
