import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  nombre: string = '';
  valor: number = 1000;
  object = {
    nombre: 'diana ysabel'
  };

  mostrarNombre = () => {
    this.nombre = 'diana ysabel';
    console.log(this.nombre);
    console.log(this.valor);
    console.log(this.object);
  };

  constructor(private primengConfig: PrimeNGConfig) {}

  
  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

}
